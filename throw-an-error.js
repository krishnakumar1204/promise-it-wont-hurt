function parsePromised(data){
    return new Promise((fulfill, reject) => {
        try {
            let JSONData = JSON.parse(data);
            fulfill(JSONData);
        } catch (error) {
            reject(error.message);
        }
    });
}

parsePromised(process.argv[2])
    .then(console.log)
    .catch(console.log)