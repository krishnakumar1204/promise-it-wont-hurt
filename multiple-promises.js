function all(promise1, promise2) {

    let ans = [];
    let cnt = 0;

    return new Promise((fulfill, reject) => {

        promise1.then((data) => {

            ans.push(data);
            cnt++;

            if (cnt === 2) {
                fulfill(ans);
            }
        });

        promise2.then((res) => {

            ans.push(res);
            fulfill(ans);

            if (cnt === 2) {
                fulfill(ans);
            }
        })
    })
}

all(getPromise1(), getPromise2()).then(console.log)