let promise = Promise.resolve('MANHATTAN');

function attachTitle(name){
    return 'DR. ' + name;
}

promise.then(attachTitle)
.then(console.log)